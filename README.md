# 安心回家

[![pipeline status](https://gitlab.com/codogo-b/back-home-safe/badges/master/pipeline.svg)](https://gitlab.com/codogo-b/back-home-safe/-/commits/master)

一個以安心出行為例嘅 PWA

## 有用連結
[//]: # (link:thc282.gitlab.io/back-home-safe)

## 聲明 [20-02-2021]

最近社會各界給予的壓力太大，決定離開這個項目，希望大家原諒

此 Project consider **DEV DONE** ，除Bug fix外將不會再有任何更新

並將在不久將來完全移除

## 聲明

我再重再再再申一次

此帖只以安心出行為例討論 PWA 相關技術

並非鼓勵任何人以此成品作任何堂食用途

引用[政府新聞處公告](https://www.info.gov.hk/gia/general/202102/16/P2021021600529.htm)

請大家遵從[大律師意見](http://cablenews.i-cable.com/ci/news/article/37/722934)

唔好被誤導，用了翻版「安心出行」，有法律風險，包括不誠實使用電腦或其他詐騙、行騙的罪行[。](https://thc282.gitlab.io/back-home-safe/)

任何人利用此作任何犯法行為，本人並不負責
## Local test

```bash
npm install
npm start
```

## 自己 Host

```bash
npm install
npm run build
```

將/build 放入自己 web server 就 okay

\*\*註：一定要用 https serve
